<?php 
include_once 'redis.php';
$has_error = false;
if (!isset($_GET['key']) || empty($redis->get(addslashes($_GET['key'])))) {?>
	<p style="color: red">Неверный ключ!</p>
	<?php exit();
} else
$item = $redis->get(addslashes($_GET['key']));
if (!empty($_POST)) {
	if (!empty($_POST['name']) && !empty($_POST['value'])) {
		$redis->del($_GET['key']);
		$redis->set(addslashes($_POST['name']), addslashes($_POST['value']));
		header("Location: /");
	} 
	$has_error = true;
}?>
<form action="" method="post">
	<?php if ($has_error):?>
		<p style="color: red">Оба поля должны быть заполнены</p>
	<?php endif;?>
	<input type="text" name="name" value="<?php echo addslashes($_GET['key'])?>"><br><br>
	<input type="text" name="value" value="<?php echo $item?>"><br><br>
	<input type="submit" value="Отправить"><br>
</form>