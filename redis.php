<?php 
require 'Predis/Autoloader.php';
Predis\Autoloader::register();

$sentinels = ['tcp://rc1c-ruxp0nfjpcazmm3r.mdb.yandexcloud.net:26379'];
$options = [
    'replication' => 'sentinel',
    'service' => 'redis274',
    'parameters' => [
        'password' => 'q1q1q1q1'
    ]
];
global $redis;
$redis = new Predis\Client($sentinels, $options);
