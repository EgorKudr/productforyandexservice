<?php 
include_once 'redis.php';
$has_error = false;
if (!isset($_GET['key']) || empty($redis->get(addslashes($_GET['key'])))) {?>
	<p style="color: red">Неверный ключ!</p>
	<?php exit();
} 
$item = $redis->del(addslashes($_GET['key']));
header("Location: /");