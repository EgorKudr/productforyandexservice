<?php 
include_once 'redis.php';
$has_error = false;
if (!empty($_POST)) {
	if (!empty($_POST['name']) && !empty($_POST['value'])) {
		$redis->set(addslashes($_POST['name']), addslashes($_POST['value']));
		header("Location: /");
	} 
	$has_error = true;

}?>
<form action="" method="post">
	<?php if ($has_error):?>
		<p style="color: red">Оба поля должны быть заполнены</p>
	<?php endif;?>
	<input type="text" name="name" value="<?php echo $_POST['name']?>"><br><br>
	<input type="text" name="value" value="<?php echo $_POST['value']?>"><br><br>
	<input type="submit" value="Отправить"><br>
</form>